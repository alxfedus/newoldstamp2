import React, { useState, useMemo } from 'react'
import Header from './Header'
import EmailTemplates from './EmailTemplates'
import data from '../../../data'
import { Layout } from 'antd'
import styled from '@emotion/styled'

const { Content } = Layout

const StyledLayout = styled(Layout)({
  display: 'flex',
  flexDirection: 'column',
  minHeight: '100vh',
})

const StyledContent = styled(Content)({
  flexGrow: 1,
  padding: '15px 50px',
})

function HomePage(props) {
  // P.S Над версткой не заморачивался, не хотел тратить время, так что строго не судите)
  const [valueInternal, setValueInternal] = useState(data)

  const selectedItemsLength = useMemo(() => 
    valueInternal.filter(i => i.isSelected).length, [valueInternal])

  const onSelect = (value, id) => {
    const array = [...valueInternal]
    const item = array.find(i => i.id === id)
    item.isSelected = value
    setValueInternal(array)
  }

  const onRemove = (id) => {
    const array = [...valueInternal]
    const index = array.findIndex(i => i.id === id)
    array.splice(index, 1)
    setValueInternal(array)
  }

  const onRemoveAll = () => 
    setValueInternal(valueInternal.filter(i => !i.isSelected))

  const onChangeAll = (value) => 
    setValueInternal(valueInternal.map(i => {
      i.isSelected = value
      return i
    }))

  return (
    <div>
      <StyledLayout>
        <Header 
          length={selectedItemsLength}
          fullLength={valueInternal.length}
          onRemoveAll={onRemoveAll}
          onChangeAll={onChangeAll}
        />
        <StyledContent>
          {Boolean(valueInternal.length) ? 
            <EmailTemplates 
              templates={valueInternal} 
              hasSelectedItem={Boolean(selectedItemsLength)}
              onRemove={onRemove}
              onSelect={onSelect}
            /> : 'No data'
          }
        </StyledContent>
      </StyledLayout>
    </div>
  )
}

export default HomePage