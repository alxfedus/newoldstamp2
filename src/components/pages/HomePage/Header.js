import React, { useMemo } from 'react'
import styled from '@emotion/styled'

import { Layout, Button, Checkbox } from 'antd'
const { Header } = Layout

const ActiveHeader = styled(Header)({
  backgroundColor: 'transparent',
  display: 'flex',
  alignItems: 'center'
})

const DefaultHeader = styled(ActiveHeader)({
  justifyContent: 'space-between',
})

const ActiveHeaderDeleteButton = styled(Button)({
  marginLeft: 50,
})

const CheckboxStyled = styled(Checkbox)({
  marginRight: 5,
})

function HeaderComponent(props) {
  const isIndeterminate = useMemo(() => {
    return Boolean(props.length && props.length < props.fullLength)
  }, [props.length, props.fullLength])

  const isChecked = useMemo(() => props.length === props.fullLength, 
    [props.length, props.fullLength])

  const renderDefaultHeader = () => {
    return (
      <DefaultHeader>
        <div>Signature List</div>
        <Button 
          href="https://www.google.com.ua/"
          target="_blank"
          type="primary" 
          ghost
        >
          Create a signature
        </Button>
      </DefaultHeader>
    )
  }
  
  const renderActiveHeader = () => {
    return (
      <ActiveHeader>
        <div>
          <CheckboxStyled 
            checked={isChecked}
            indeterminate={isIndeterminate}
            onChange={e => {
              const payload = isIndeterminate ? true : e.target.checked
              props.onChangeAll(payload)
            }} 
          />
          Selected {`(${props.length})`}
        </div>
        <ActiveHeaderDeleteButton 
          type="primary" 
          icon="delete"
          ghost
          onClick={props.onRemoveAll}
        >
          Delete {`(${props.length})`}
        </ActiveHeaderDeleteButton>
      </ActiveHeader>
    )
  }

  return (
    <>
      {Boolean(props.length) ? 
        renderActiveHeader() : renderDefaultHeader()}
    </>
  )
}

export default HeaderComponent