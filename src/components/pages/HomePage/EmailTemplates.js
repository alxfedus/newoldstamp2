import React from 'react'
import EmailTemplate from './EmailTemplate'
import styled from '@emotion/styled'

const EmailTemplatesStyled = styled.div({
  display: 'grid',
  gridGap: 15,
  gridTemplateColumns: 'repeat(auto-fit, minmax(500px, 1fr))'
})

function EmailTemplates(props) {
  return (
    <EmailTemplatesStyled>
      {props.templates.map(template => (
        <EmailTemplate 
          key={template.id}
          item={template}
          hasSelectedItem={props.hasSelectedItem}
          onSelect={props.onSelect}
          onRemove={props.onRemove}
        />
      ))}
    </EmailTemplatesStyled>
  )
}

export default EmailTemplates