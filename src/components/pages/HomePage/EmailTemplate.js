//import React from 'react'
import { Button, Checkbox } from 'antd';
import styled from '@emotion/styled'

/** @jsx jsx */
import { jsx } from '@emotion/core'

const activeOverlayStyles = {
  opacity: 1,
  visibility: 'visible',
  transform: 'scale(1)',
}

const EmailTemplateStyled = styled.div(props => ({
  position: 'relative',
  paddingTop: '45%',
  borderRadius: '4px',
  border: '1px solid grey',
  backgroundImage: `url(${props.src})`,
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  ':hover > div': activeOverlayStyles
}))

const Overlay = styled.div({
  cursor: 'pointer',
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  display: 'flex',
  flexDirection: 'column',
  padding: 10,
  backgroundColor: 'rgba(0, 75, 185, 0.9)',
  opacity: 0,
  visibility: 'hidden',
  transform: 'scale(0.5)',
  transition: '.2s ease-out',
})

const TextBox = styled.div(props => ({
  display: props.hasSelectedItem ? 'none' : 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexGrow: 1,
  color: 'white',
}))

const ActionsBox = styled.div(props => ({
  display: props.hasSelectedItem ? 'none' : 'flex',
  justifyContent: 'space-between',
  cursor: 'auto',
}))

const Actions = styled.div({
  display: 'grid',
  gridGap: 10,
  gridTemplateColumns: '1fr 1fr 1fr'
})

const StyledButton = styled(Button)({
  '.anticon': {
    paddingBottom: 2,
  }
})

function EmailTemplate({ item, hasSelectedItem, onSelect, onRemove }) {
  return (
    <EmailTemplateStyled src={item.src}>
      <Overlay 
        css={hasSelectedItem && activeOverlayStyles}
        onClick={() => onSelect(!item.isSelected, item.id)}
      >
        <Checkbox 
          checked={item.isSelected}
          onChange={e => e.preventDefault()}
        />
        <TextBox hasSelectedItem={hasSelectedItem}>
          {item.text}
        </TextBox>
        <ActionsBox 
          hasSelectedItem={hasSelectedItem}
          onClick={e => e.stopPropagation()}
        >
          <Button 
            href="https://www.google.com.ua/"
            target="_blank"
            ghost
          >
            Use
          </Button>
          <Actions>
            <StyledButton type="primary" shape="circle" icon="edit" disabled />
            <StyledButton type="primary" shape="circle" icon="eye" disabled />
            <StyledButton 
              type="primary" 
              shape="circle" 
              icon="delete" 
              onClick={() => onRemove(item.id)} 
            />
          </Actions>
        </ActionsBox>
      </Overlay>
    </EmailTemplateStyled>
  )
}

export default EmailTemplate