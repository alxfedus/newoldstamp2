import Image1 from '../images/1.jpg'
import Image2 from '../images/2.jpg'
import Image3 from '../images/3.jpg'
import Image4 from '../images/4.jpg'
import Image5 from '../images/5.jpg'

export default [
  { 
    id: 1, 
    src: Image1,
    text: 'default text',
    isSelected: false, 
  },
  { 
    id: 2, 
    src: Image2, 
    text: 'default text',
    isSelected: false,
  },
  { 
    id: 3, 
    src: Image3, 
    text: 'default text',
    isSelected: false,
  },
  { 
    id: 4, 
    src: Image4,
    text: 'default text',
    isSelected: false, 
  },
  { 
    id: 5, 
    src: Image5, 
    text: 'default text',
    isSelected: false,
  },
  { 
    id: 6, 
    src: Image4, 
    text: 'default text',
    isSelected: false,
  },
  { 
    id: 7, 
    src: Image2,
    text: 'default text',
    isSelected: false, 
  },
  { 
    id: 8, 
    src: Image3, 
    text: 'default text',
    isSelected: false,
  },
  { 
    id: 9, 
    src: Image1, 
    text: 'default text',
    isSelected: false,
  },
]